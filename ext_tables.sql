CREATE TABLE tt_content (
    backendtitle varchar(255) DEFAULT '' NOT NULL,
    tx_tt3swipernce_configuration tinytext DEFAULT 'default',
    tx_tt3swipernce_sliderautoplay tinytext,
    tx_tt3swipernce_sliderautoplay_disableoninteraction tinyint(4) DEFAULT '1' NOT NULL,
    tx_tt3swipernce_sliderautoplay_pauseonmouseenter tinyint(4) DEFAULT '0' NOT NULL,
    tx_tt3swipernce_sliderhashnavigation tinyint(4) DEFAULT '0' NOT NULL,
    tx_tt3swipernce_sliderloop tinyint(4) DEFAULT '0' NOT NULL,
    tx_tt3swipernce_sliderpagination tinytext,
    tx_tt3swipernce_slidershowarrows tinyint(4) DEFAULT '0' NOT NULL,
    tx_tt3swipernce_slide int(11) unsigned DEFAULT '0' NOT NULL,
    tx_tt3swipernce_slide_content_parent int(11) unsigned DEFAULT '0' NOT NULL,
    KEY tx_tt3swipernce_slide_content_parent (tx_tt3swipernce_slide_content_parent,pid,deleted),
);
CREATE TABLE tx_tt3swipernce_slide (
    parentid int(11) DEFAULT '0' NOT NULL,
    parenttable varchar(255) DEFAULT '' NOT NULL,
    sorting int(11) unsigned DEFAULT '0' NOT NULL,
    slide_backendtitle tinytext,
    slide_bgimage int(11) unsigned DEFAULT '0' NOT NULL,
    slide_bgposition tinytext,
    slide_content int(11) unsigned DEFAULT '0' NOT NULL,
    slide_cssclass tinytext,
    slide_paginationtitle tinytext,
    KEY language (l10n_parent,sys_language_uid)
);
