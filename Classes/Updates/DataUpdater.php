<?php

declare(strict_types=1);

/*
 * This file is part of the "tt3_swiper_nce" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Teufels\Tt3SwiperNce\Updates;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

#[UpgradeWizard('tt3swipernceDataUpdater')]
class DataUpdater implements UpgradeWizardInterface
{

    public function getTitle(): string
    {
        return '[teufels] Swiper NCE: Migrate data';
    }

    public function getDescription(): string
    {
        $description = 'This update wizard migrates all data from tx_hiveswiperjssimple_sliderelement to the new tx_tt3swipernce_slide';
        if($this->checkMigrationTableExist()) { $description .= ': ' . count($this->getMigrationRecords()); }
        return $description;
    }

    public function getPrerequisites(): array
    {
        return [
            DatabaseUpdatedPrerequisite::class,
        ];
    }

    public function updateNecessary(): bool
    {
        return $this->checkIfWizardIsRequired();
    }

    public function executeUpdate(): bool
    {
        return $this->performMigration();
    }

    public function checkIfWizardIsRequired(): bool
    {
        return $this->checkMigrationTableExist();
    }

    public function performMigration(): bool
    {
        //data
        $sql = "INSERT INTO tx_tt3swipernce_slide(uid,pid,tstamp,crdate,deleted,hidden,starttime,endtime,fe_group,editlock,sys_language_uid,l10n_parent,l10n_source,t3_origuid,parentid,parenttable,sorting,slide_backendtitle,slide_bgimage,slide_bgposition,slide_content,slide_cssclass,slide_paginationtitle) 
        SELECT uid,pid,tstamp,crdate,deleted,hidden,starttime,endtime,fe_group,editlock,sys_language_uid,l10n_parent,l10n_source,t3_origuid,parentid,parenttable,sorting,tx_hiveswiperjssimple_elementbackendtitle,tx_hiveswiperjssimple_elementbgimage,tx_hiveswiperjssimple_elementbgposition,tx_hiveswiperjssimple_elementcontent,tx_hiveswiperjssimple_elementcssclass,tx_hiveswiperjssimple_elementpaginationtitle
        FROM tx_hiveswiperjssimple_sliderelement WHERE deleted = 0";

        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_tt3swipernce_slide');
        /** @var DriverStatement $statement */
        $statement = $connection->prepare($sql);
        $statement->execute();

        //sys_file_reference
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('sys_file_reference');
        $queryBuilder
            ->update('sys_file_reference')
            ->set('tablenames', 'tx_tt3swipernce_slide')
            ->set('fieldname', 'slide_bgimage')
            ->where(
                $queryBuilder->expr()->eq('tablenames', $queryBuilder->createNamedParameter('tx_hiveswiperjssimple_sliderelement'))
            )
            ->executeStatement();

        //Nested CE
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tt_content');
        $queryBuilder
            ->update('tt_content')
            ->set('tx_tt3swipernce_slide_content_parent',$queryBuilder->quoteIdentifier('tx_hiveswiperjssimple_elementcontent_parent'),false)
            ->where(
                $queryBuilder->expr()->gt('tx_hiveswiperjssimple_elementcontent_parent', 0)
            )
            ->executeStatement();

        return true;
    }

    protected function getMigrationRecords(): array
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_hiveswiperjssimple_sliderelement');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        return $queryBuilder
            ->select('uid')
            ->from('tx_hiveswiperjssimple_sliderelement')
            ->executeQuery()
            ->fetchAllAssociative();
    }

    protected function checkMigrationTableExist(): bool {
        return GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_hiveswiperjssimple_sliderelement')
            ->getSchemaManager()
            ->tablesExist(['tx_hiveswiperjssimple_sliderelement']);
    }

}
