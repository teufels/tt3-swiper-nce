<?php
declare(strict_types=1);

namespace Teufels\Tt3SwiperNce\EventListener;

// use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Backend\View\Event\IsContentUsedOnPageLayoutEvent as OriginalIsContentUsedOnPageLayoutEvent;

class IsContentUsedOnPageLayoutEvent {
    public function __invoke(OriginalIsContentUsedOnPageLayoutEvent $event): void {

        // Hide Child Content Element at module "Page" at the backend
        if($event->isRecordUsed() === false && $event->getRecord()['CType'] !== 'tt3swipernce_tt3_swiper_nce' && $event->getRecord()['colPos'] === 999) {
            $event->setUsed(true);
        }
    }
}
