<?php
defined('TYPO3') or die();

$extensionKey = 'tt3_swiper_nce';
$extensionTitle = '[teufels] Swiper NCE';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extensionKey, 'Configuration/TypoScript', $extensionTitle);