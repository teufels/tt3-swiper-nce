<?php
defined('TYPO3') || die();

call_user_func(function () {

    //Adding Custom CType Item Group
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItemGroup(
        'tt_content',
        'CType',
        'teufels',
        'teufels',
        'after:special'
    );

    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['tt3swipernce_tt3_swiper_nce'] = 'tt3swipernce_plugin_icon';
    $tempColumns = [
        'backendtitle' => [
            'config' => [
                'autocomplete' => '0',
                'behaviour' => [
                    'allowLanguageSynchronization' => false,
                ],
                'type' => 'input',
            ],
            'description' => 'Backend Title (not visible in frontend)',
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.backendtitle',
        ],

        'tx_tt3swipernce_configuration' => [
            'config' => [
                'renderType' => 'selectSingle',
                'type' => 'select',
                'behaviour' => [
                    'allowLanguageSynchronization' => false,
                ],
                'items' => [
                    [
                        'label' => 'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_configuration.I.0',
                        'value' => 'default',
                    ],
                ],
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_configuration',
        ],

        'tx_tt3swipernce_sliderautoplay' => [
            'config' => [
                'default' => '0',
                'eval' => 'int',
                'type' => 'input',
                'behaviour' => [
                    'allowLanguageSynchronization' => false,
                ],
            ],
            'description' => 'Autoplay Delay in ms (0 = disabled)',
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_sliderautoplay',
        ],
        'tx_tt3swipernce_sliderautoplay_disableoninteraction' => [
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 1,
                'items' => [
                    [
                        0 => '',
                        1 => '',
                    ]
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => false,
                ],
            ],
            'description' => 'Set to false and autoplay will not be disabled after user interactions (swipes), it will be restarted every time after interaction',
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_sliderautoplay_disableoninteraction',
        ],
        'tx_tt3swipernce_sliderautoplay_pauseonmouseenter' => [
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 0,
                'items' => [
                    [
                        0 => '',
                        1 => '',
                    ]
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => false,
                ],
            ],
            'description' => 'When enabled autoplay will be paused on pointer (mouse) enter over Swiper container.',
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_sliderautoplay_pauseonmouseenter',
        ],

        'tx_tt3swipernce_slide' => [
            'config' => [
                'appearance' => [
                    'collapseAll' => true,
                    'enabledControls' => [
                        'dragdrop' => true,
                    ],
                    'expandSingle' => true,
                    'levelLinksPosition' => 'both',
                    'showAllLocalizationLink' => true,
                    'showPossibleLocalizationRecords' => true,
                    'showSynchronizationLink' => true,
                    'useSortable' => true,
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'foreign_field' => 'parentid',
                'foreign_sortby' => 'sorting',
                'foreign_table' => 'tx_tt3swipernce_slide',
                'foreign_table_field' => 'parenttable',
                'type' => 'inline',
                'minitems' => 1,
                'maxitems' => 99,
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_slide',
        ],
        'tx_tt3swipernce_sliderhashnavigation' => [
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 0,
                'items' => [
                    [
                        0 => '',
                        1 => '',
                    ]
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => false,
                ],
            ],
            'description' => 'Enables hash url navigation for slides',
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_sliderhashnavigation',
        ],
        'tx_tt3swipernce_sliderloop' => [
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 0,
                'items' => [
                    [
                        0 => '',
                        1 => '',
                    ]
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => false,
                ],
            ],
            'description' => 'Loop Mode / Infinite Loop',
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_sliderloop',
        ],
        'tx_tt3swipernce_sliderpagination' => [
            'config' => [
                'items' => [
                    [
                        'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_sliderpagination.default',
                        'default',
                    ],
                    [
                        'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_sliderpagination.dynamicbullets',
                        'dynamicbullets',
                    ],
                    [
                        'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_sliderpagination.fraction',
                        'fraction',
                    ],
                    [
                        'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_sliderpagination.progressbar',
                        'progressbar',
                    ],
                    [
                        'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_sliderpagination.scrollbar',
                        'scrollbar',
                    ],
                    [
                        'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_sliderpagination.scrollbar-fraction',
                        'scrollbar-fraction',
                    ],
                    [
                        'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_sliderpagination.custom',
                        'custom',
                    ],
                    [
                        'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_sliderpagination.nopagination',
                        'nopagination',
                    ],
                ],
                'renderType' => 'selectSingle',
                'type' => 'select',
                'behaviour' => [
                    'allowLanguageSynchronization' => false,
                ],
            ],
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_sliderpagination',
        ],
        'tx_tt3swipernce_slidershowarrows' => [
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 1,
                'items' => [
                    [
                        0 => '',
                        1 => '',
                    ]
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => false,
                ],
            ],
            'description' => 'show navigation arrows',
            'exclude' => '1',
            'label' => 'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.tx_tt3swipernce_slidershowarrows',
        ],
    ];
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);

    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
        'LLL:EXT:tt3_swiper_nce/Resources/Private/Language/locallang_db.xlf:tt_content.CType.tt3swipernce_tt3_swiper_nce',
        'tt3swipernce_tt3_swiper_nce',
        // Icon
        'tt3swipernce_plugin_icon',
        // The group ID
        'teufels'
    ];

    // Define the palette
    $GLOBALS['TCA']['tt_content']['palettes']['palette_tx_tt3swipernce_sliderautoplay'] = [
        'showitem' => '
            tx_tt3swipernce_sliderautoplay, 
            tx_tt3swipernce_sliderautoplay_disableoninteraction, 
            tx_tt3swipernce_sliderautoplay_pauseonmouseenter
        ',
    ];

    $tempTypes = [
        'tt3swipernce_tt3_swiper_nce' => [
            'showitem' => '
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                backendtitle,
                tx_tt3swipernce_configuration,
                tx_tt3swipernce_slide,
            --div--;Options,
                tx_tt3swipernce_sliderpagination,
                --palette--;;palette_tx_tt3swipernce_sliderautoplay,
                tx_tt3swipernce_slidershowarrows,
                tx_tt3swipernce_sliderloop,
                tx_tt3swipernce_sliderhashnavigation,
            --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,--palette--;;hidden,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
            --div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,
                categories,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
                rowDescription,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended
        ',
        ],
    ];
    $GLOBALS['TCA']['tt_content']['types'] += $tempTypes;

    /**
     * Preview Renderer (needed for TYPO3 v12 backwards compatibility)
     */
    $GLOBALS['TCA']['tt_content']['types']['tt3swipernce_tt3_swiper_nce']['previewRenderer'] = \Teufels\Tt3SwiperNce\Preview\PreviewRenderer::class;
});

