[![VENDOR](https://img.shields.io/badge/vendor-teufels-blue.svg)](https://bitbucket.org/teufels/workspace/projects/TTER)
[![PACKAGE](https://img.shields.io/badge/package-tt3--swiper--nce-orange.svg)](https://bitbucket.org/teufels/tt3-swiper-nce/src/main/)
[![KEY](https://img.shields.io/badge/extension--key-tt3__swiper__nce-red.svg)](https://bitbucket.org/teufels/tt3-swiper-nce/src/main/)
![version](https://img.shields.io/badge/version-1.2.*-yellow.svg?style=flat-square)

[ ṯeufels ] Swiper NCE
==========
Slider based on Swiper JS (https://swiperjs.com/) allows to use nested content elements in Slide

#### This version supports TYPO3
![CUSTOMER](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)
![CUSTOMER](https://img.shields.io/badge/13_LTS-%23A6C694.svg?style=flat-square)

#### Composer support
`composer req teufels/tt3-swiper-nce`

***

### Swiper.js Documents
  * [Getting Started Guide](https://swiperjs.com/get-started/)
  * [API](https://swiperjs.com/api/)
  * [Demos](https://swiperjs.com/demos/)

***

### Requirements
`Swiper.js: >=6.8.4`

***

### How to use
- Install with composer
- Import Static Template (before sitepackage)
- make own Configurations
  - add Configurations to TsConfig/Page/TCEFORM.tsconfig in sitepackage
  - add own configuration js file to sitepackage & implement these in setup.typoscript of sitepackage

***

### Update & Migration from hive_swiperjs_simple
1. in composer.json replace `beewilly/hive_swiperjs_simple` with `"teufels/tt3-swiper-nce":"^1.0"`
2. Composer update
3. Include TypoScript set `[teufels] Swiper NCE`
4. Analyze Database Structure -> Add tables & fields (do not remove old hive_swiperjs_simple yet)
5. Perform Upgrade Wizards `[teufels] Swiper NCE`
6. Analyze Database Structure -> Remove tables & unused fields (remove old hive_swiperjs_simple now)
7. class & id changed -> adjust styling in sitepackage (e.g. tx_tt3_swiper_nce => tx_tt3_swiper_nce)
8. check & adjust be user group access rights
9. migrate own configurations

***

### Changelog
#### 1.2.x
- add support for TYPO3 v13
  - Fix: Adaptation to TYPO3 v13 - Use of TYPO3\CMS\Core\Database\Connection:PARAM_INT instead of \PDO::PARAM_INT
  - custom preview renderer only used for TYPO3 v12, TYPO3 v13 uses default renderer
  - add Previews for TYPO3 v13 and TYPO3 v12 support
  - remove usage of vhs:condition.string.contains (not TYPO3v13 ready yet -27.01.25-) using simple condition instead
#### 1.1.x
- 1.1.1 fix autoplay 
- 1.1.0 New Features
  - add Autoplay Options disableOnInteraction & pauseOnMouseEnter
  - add Scrollbar & Scrollbar+Fraction PaginationType
#### 1.0.x
- 1.0.5 fix renderContentElementPreviewFromFluidTemplate changed parameters
- 1.0.4 change c-<id> to c<id> & improve BE Preview
- 1.0.3 change TCA Config 'slide_content 'according to news configuration 'content_elements'
- 1.0.2 changed deprecated allowTableOnStandardPages() to ignorePageTypeRestriction (https://docs.typo3.org/m/typo3/reference-tca/12.4/en-us/Ctrl/Properties/Security.html#ctrl-security-ignorepagetyperestriction)
- 1.0.1 fix pagination problems with no-pagination [JIRA TASK](https://teufels.atlassian.net/browse/TTER-7)
- 1.0.0 intial from [hive_facts](https://bitbucket.org/teufels/hive_swiperjs_simple/src/)