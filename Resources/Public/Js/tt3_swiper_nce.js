$( document ).ready( function() {
    var aSwiper = [];
    var titles = [];

    $( ".tx_tt3_swiper_nce .swiper-container[data-configuration='default']" ).each(function( index ) {

        // Initialize Configuration
        var $uid = $( this ).data("uid");
        var $autoplay = parseInt($( this ).data("autoplay"));
        var $autoplayDisableOnInteraction = parseInt($( this ).data("autoplay-disableoninteraction"));
        var $autoplayPauseOnMouseEnter = parseInt($( this ).data("autoplay-pauseonmouseenter"));
        var $loop = parseInt($( this ).data("loop"));
        var $hashNavigation = parseInt($( this ).data("hash-navigation"));
        if($hashNavigation == 1) { $hashNavigation = true; }
        var $paginationType = $( this ).data("pagination-type");

        // Initialize Swiper
        console.log("[teufels] Swiper NCE :: Swiper initialize #tt3-swiper-nce--" + $uid);
        var swiperIdentifier = '#tt3-swiper-nce--' + $uid;
        aSwiper[index] = new Swiper('#tt3-swiper-nce--' + $uid, {

            loop: $loop,
            hashNavigation: $hashNavigation,
            grabCursor: true,

            pagination: {
                el: '#swiper-pagination--' + $uid,
                clickable: true,
            },
            scrollbar: {
                el: '#swiper-scrollbar--' + $uid,
                draggable: true,
            },

            navigation: {
                nextEl: '#swiper-button-next--' + $uid,
                prevEl: '#swiper-button-prev--' + $uid,
            },

        });

        /**
         * update Swiper (add Params)
         **/

        //autoplay
        if($autoplay > 0) {
            aSwiper[index].params.autoplay.delay = $autoplay;
            aSwiper[index].params.autoplay.disableOnInteraction = $autoplayDisableOnInteraction;
            aSwiper[index].params.autoplay.pauseOnMouseEnter = $autoplayPauseOnMouseEnter;
            aSwiper[index].autoplay.start();
        }

        //pagination
        switch ($paginationType) {
            case 'dynamicbullets':
                aSwiper[index].params.pagination.type = 'bullets';
                aSwiper[index].params.pagination.dynamicBullets = true;
                aSwiper[index].pagination.init();
                aSwiper[index].pagination.render();
                aSwiper[index].pagination.update();
                break;
            case 'fraction':
                aSwiper[index].params.pagination.type = 'fraction';
                aSwiper[index].pagination.init();
                aSwiper[index].pagination.render();
                aSwiper[index].pagination.update();
                break;
            case 'progressbar':
                aSwiper[index].params.pagination.type = 'progressbar';
                aSwiper[index].pagination.init();
                aSwiper[index].pagination.render();
                aSwiper[index].pagination.update();
                break;
            case 'scrollbar':
                aSwiper[index].pagination.destroy();
                aSwiper[index].pagination.render();
                aSwiper[index].pagination.update();
                break;
            case 'scrollbar-fraction':
                aSwiper[index].params.pagination.type = 'fraction';
                aSwiper[index].params.pagination.clickable = false;
                aSwiper[index].params.pagination.renderFraction = function (currentClass, totalClass) {
                    return '<span class="' + currentClass + '"></span>' +
                        ' | ' + '<span class="' + totalClass + '"></span>';
                }
                aSwiper[index].pagination.init();
                aSwiper[index].pagination.render();
                aSwiper[index].pagination.update();
            case 'custom':
                aSwiper[index].params.pagination.clickable = true;
                aSwiper[index].params.pagination.renderBullet = function (index, className) {
                    var el =  $( swiperIdentifier ).find(".slide--" + (index + 1) + ":not(.swiper-slide-duplicate)");
                    var titleValue = el.data("pagination-title").toString();
                    if (!titles.includes(titleValue)) {
                        titles.push(titleValue);
                        return '<span class="' + className + ' swiper-pagination-custom swiper-pagination-titled">' + titles[index] + '</span>';
                    }
                    return "";
                };
                aSwiper[index].pagination.init();
                aSwiper[index].pagination.render();
                aSwiper[index].pagination.update();
                break;
            case 'nopagination':
                aSwiper[index].pagination.destroy();
                aSwiper[index].pagination.render();
                aSwiper[index].pagination.update();
                break;
            default:
                aSwiper[index].params.pagination.type = 'bullets';
                aSwiper[index].params.pagination.dynamicBullets = false;
                aSwiper[index].pagination.init();
                aSwiper[index].pagination.render();
                aSwiper[index].pagination.update();
                break;
        }

        aSwiper[index].update();

    });

});